class Frame < ActiveRecord::Base
	belongs_to :work

	validates :registration_number1, presence: true,length: { maximum: 4 }
	validates :registration_number2, presence: true,length: { maximum: 1 }
	validates :registration_number3, presence: true,numericality: true ,length: { maximum: 4 }
	validates :frame_number1, presence: true,length: { maximum: 4 }
	validates :frame_number2, presence: true,numericality: true,length: { maximum: 7 }

	def frame_number
		self.frame_number1 + '-' + self.frame_number2 
  end

end
