class Work < ActiveRecord::Base
	has_many :contents
	has_one :shop
	has_one :frame
	accepts_nested_attributes_for :contents, allow_destroy: true
end
