json.array!(@frames) do |frame|
  json.extract! frame, :id, :id, :registration_number1, :registration_number2, :registration_number3, :frame_number1, :frame_number2, :created, :modified
  json.url frame_url(frame, format: :json)
end
