json.array!(@shops) do |shop|
  json.extract! shop, :id, :id, :name, :created, :modified
  json.url shop_url(shop, format: :json)
end
