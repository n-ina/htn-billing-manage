json.array!(@works) do |work|
  json.extract! work, :id, :id, :shop_id, :work_date, :work_segment, :frame_id, :mileage, :created, :modified
  json.url work_url(work, format: :json)
end
