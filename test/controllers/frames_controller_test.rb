require 'test_helper'

class FramesControllerTest < ActionController::TestCase
  setup do
    @frame = frames(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:frames)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create frame" do
    assert_difference('Frame.count') do
      post :create, frame: { created: @frame.created, frame_number1: @frame.frame_number1, frame_number2: @frame.frame_number2, id: @frame.id, modified: @frame.modified, registration_number1: @frame.registration_number1, registration_number2: @frame.registration_number2, registration_number3: @frame.registration_number3 }
    end

    assert_redirected_to frame_path(assigns(:frame))
  end

  test "should show frame" do
    get :show, id: @frame
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @frame
    assert_response :success
  end

  test "should update frame" do
    patch :update, id: @frame, frame: { created: @frame.created, frame_number1: @frame.frame_number1, frame_number2: @frame.frame_number2, id: @frame.id, modified: @frame.modified, registration_number1: @frame.registration_number1, registration_number2: @frame.registration_number2, registration_number3: @frame.registration_number3 }
    assert_redirected_to frame_path(assigns(:frame))
  end

  test "should destroy frame" do
    assert_difference('Frame.count', -1) do
      delete :destroy, id: @frame
    end

    assert_redirected_to frames_path
  end
end
