# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150719161635) do

  create_table "contents", force: :cascade do |t|
    t.integer  "work_id",      limit: 4
    t.text     "content",      limit: 65535
    t.string   "content_type", limit: 255
    t.integer  "quantity",     limit: 4
    t.integer  "parts_cost",   limit: 4
    t.integer  "man_hour",     limit: 4
    t.integer  "pay",          limit: 4
    t.datetime "created"
    t.datetime "modified"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "frames", force: :cascade do |t|
    t.string   "registration_number1", limit: 255
    t.string   "registration_number2", limit: 255
    t.string   "registration_number3", limit: 255
    t.string   "frame_number1",        limit: 255
    t.string   "frame_number2",        limit: 255
    t.datetime "created"
    t.datetime "modified"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "shops", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created"
    t.datetime "modified"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "works", force: :cascade do |t|
    t.integer  "shop_id",      limit: 4
    t.datetime "work_date"
    t.integer  "work_segment", limit: 4
    t.integer  "frame_id",     limit: 4
    t.integer  "mileage",      limit: 4
    t.datetime "created"
    t.datetime "modified"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

end
