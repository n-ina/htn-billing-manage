class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.integer :work_id
      t.text :content
      t.string :content_type
      t.integer :quantity
      t.integer :parts_cost
      t.integer :man_hour
      t.integer :pay

      t.timestamps null: false
    end
  end
end
