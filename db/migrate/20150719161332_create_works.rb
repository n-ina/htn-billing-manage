class CreateWorks < ActiveRecord::Migration
  def change
    create_table :works do |t|
      t.integer :shop_id
      t.datetime :work_date
      t.integer :work_segment
      t.integer :frame_id
      t.integer :mileage

      t.timestamps null: false
    end
  end
end
