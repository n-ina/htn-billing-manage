class CreateFrames < ActiveRecord::Migration
  def change
    create_table :frames do |t|
      t.string :registration_number1
      t.string :registration_number2
      t.string :registration_number3
      t.string :frame_number1
      t.string :frame_number2

      t.timestamps null: false
    end
  end
end
